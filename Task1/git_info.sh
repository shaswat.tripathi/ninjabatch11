#!/bin/bash

BIN
URL=$1
DAYS=$2

git clone ${URL}

echo ${URL} > url
FILE=`awk -F"/" '{print $5}' url | cut -f1 -d.`

cd $FILE

git log --pretty=format:"%ad | %an | %ae | %s" --since=${DAYS}.days
